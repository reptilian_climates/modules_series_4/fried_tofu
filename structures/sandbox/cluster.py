


'''
	rethinkdb proxy --join 127.0.0.1:29016 --daemon
	
	rethinkdb -o 1 --bind all --daemon
		# Listening for intracluster connections on port 29016
		# Listening for client driver connections on port 28016
		# Listening for administrative HTTP connections on port 8081
	
	rethinkdb -o 2 --join 127.0.0.1:29016 --daemon
	rethinkdb -o 3 --join 127.0.0.1:29016 --daemon
'''


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../modules',
	'../modules_pip'
])


import fried_tofu.cluster.start as start_cluster
ly = start_cluster.attractively (
	proxy = {
		"ports": []
	},
	nodes = [{
		"ports": []
	},{
		"ports": []
	},{
		"ports": []
	}]
)