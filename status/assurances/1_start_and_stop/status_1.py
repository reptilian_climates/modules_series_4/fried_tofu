


'''
	python3 insurance.py "1_start_and_stop/status_1.py"
'''

ports = [ 55192, 55193, 55194 ]


import atexit
import shutil
import pathlib
from os.path import dirname, join, normpath
import os

import botany
import fried_tofu.node.stop as node_stop

def check_1 ():
	this_directory = pathlib.Path (__file__).parent.resolve ()	

	process_ID_file_path = str (normpath (join (this_directory, "soy.pid")))
	node_directory = str (this_directory) + "/rethinkdb_data"

	import datetime
	#timestamp = datetime.datetime ().timestamp()
	timestamp = ''.join (str (datetime.datetime.now().timestamp()).split ('.'))

	print ('timestamp:', timestamp)

	driver_port = ports [0]
	import fried_tofu.node.start as start_node
	ly = start_node.attractively (
		proxy = "no",
		ports = {
			"driver": driver_port,
			"cluster": ports [1],
			"http": ports [2]	
		},
		process = {
			"cwd": pathlib.Path (__file__).parent.resolve ()
		},
		rethink_params = [
			f"--daemon",
			f"--pid-file { process_ID_file_path }",
			f"--server-name node-1" 
		]
	)		

	import time
	time.sleep (1)

	node_stop.attractively (
		process_ID_file_path,
		driver_port = driver_port
	)
	
	
checks = {
	'check 1': check_1
}