
'''
	python3 insurance.py "2_multiple_nodes/status_1.py"
'''

ports = [ 
	55195, 55196, 55197,
	55198, 55199, 55200, 
	55201, 55202, 55203	
]

import atexit
import shutil
import pathlib
from os.path import dirname, join, normpath
import os

import botany
import fried_tofu.node.stop as node_stop
import fried_tofu.node.start as start_node

import botany.flow.demux_mux2 as demux_mux2

this_directory = pathlib.Path (__file__).parent.resolve ()	
cluster_directory = normpath (join (this_directory, "cluster"))

nodes = [
	[ 
		str (normpath (join (cluster_directory, "node-1"))),
		[ 55195, 55196, 55197 ]
	],
	[
		str (normpath (join (cluster_directory, "node-2"))),
		[ 55198, 55199, 55200 ]
	],
	[
		str (normpath (join (cluster_directory, "node-3"))),
		[ 55201, 55202, 55203 ]
	]
]

def check_1 ():
	shutdown_list = []

	def start (parameters):
		node_path = parameters [0]
		ports = parameters [1]
	
		os.makedirs (node_path, exist_ok = True)
	
		process_ID_file_path = str (normpath (join (node_path, "tofu.pid")))

		driver_port = ports [0]
		import fried_tofu.node.start as start_node
		ly = start_node.attractively (
			proxy = "no",
			ports = {
				"driver": driver_port,
				"cluster": ports [1],
				"http": ports [2]	
			},
			process = {
				"cwd": node_path
			},
			rethink_params = [
				f"--daemon",
				f"--pid-file { process_ID_file_path }"
			]
		)
		
		shutdown_list.append ({
			"process_ID_file_path": process_ID_file_path,
			"driver_port": driver_port
		})

	proceeds_statement = demux_mux2.start (
		start, 
		nodes
	)

	import time
	time.sleep (1)

	def stop (parameters):
		process_ID_file_path = parameters ["process_ID_file_path"]
		driver_port = parameters ["driver_port"]
	
		node_stop.attractively (
			process_ID_file_path,
			driver_port = driver_port
		)
		
	proceeds_statement = demux_mux2.start (
		stop, 
		shutdown_list
	)

	return;
	
	
checks = {
	'check 1': check_1
}