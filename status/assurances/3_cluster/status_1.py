



'''
	python3 insurance.py "3_cluster/status_1.py"
'''

proxy_ports = [ 55204, 55205, 55206 ]
node_1_ports = [ 55207, 55208, 55209 ]
node_2_ports = [ 55210, 55211, 55212 ]
node_3_ports = [ 55213, 55214, 55215 ]

'''
,{
	"ports": node_2_ports
},{
	"ports": node_3_ports
}
'''

import pathlib
import fried_tofu.cluster.start as start_cluster

def check_1 ():
	import pathlib
	from os.path import dirname, join, normpath
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	cluster_directory = normpath (join (this_directory, "tofu_cluster"))
	
	ly = start_cluster.attractively (
		cluster_directory = cluster_directory,
		proxy = {
			"ports": proxy_ports
		},
		nodes = [{
			"ports": node_1_ports
		}]
	)

	
checks = {
	'check 1': check_1
}